import React from 'react';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import Home from './containers/home';
import Profile from './containers/profile';

function Estratega() {
  return (
    <BrowserRouter>
        <Route path="/profile" component={Profile} />
        <Route path="/home" component={Home} />
        <Redirect exact from="/" to="home" />
      
    </BrowserRouter>
  );
}

export default Estratega;