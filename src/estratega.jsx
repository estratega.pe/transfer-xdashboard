import React from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { alertActions } from './actions';
import { history } from './helpers';
import { PrivateRoute } from './components';

//import { Home, Login, Register} from './containers';
import Home from './containers/home';
import Login from './containers/login';
import Register from './containers/register';

class Estratega extends React.Component {
    constructor(props) {
        super(props);

        history.listen((location, action) => {
            this.props.clearAlerts();
        });
    }

    render() {
        const {alert }  = this.props;

        return(
            <React.Fragment>
                {alert.message &&
                    <div className={`alert ${alert.type}`}>{alert.message}</div>
                }
                <Router history={history}>
                    <Switch>
                        <PrivateRoute exact path="/" component={Home} />
                        <Route path="/login" component={Login} />
                        <Route path="/register" component={Register} />
                        <Redirect from="*" to="/" />
                    </Switch>
                </Router>
            </React.Fragment>
        )
    }
}

function mapState(state) {
    const { alert } = state;
    return { alert };
}

const actionCreators = {
    clearAlerts: alertActions.clear
};

const connectedApp = connect(mapState, actionCreators)(Estratega);
export { connectedApp as Estratega };