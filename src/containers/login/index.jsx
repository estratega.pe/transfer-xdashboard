import React, { useState } from 'react';
//import { connect } from 'react-redux';

import { ThemeLayout } from '../../theme/estratega/layout';
import { ThemeLogin } from '../../theme/estratega/login';

import { userActions } from '../../actions';

const Login = () => {
    const [submitted, setSubmitted] = useState(false);
    const [_username, setUsername] = useState(false);
    const [_password, setPassword] = useState(false);

    const onSubmitLogin = (e) => {
        e.preventDefault();
        setSubmitted(true);
        userActions.login(_username, _password);
    }

    const onHandleChange = (e) => {
        const { name, value } = e.target;
        if (name === "_username")  setUsername(value) 
        if (name === "_password")  setPassword(value) 
    }

    return (
        <React.Fragment>
            <ThemeLayout>
                <ThemeLogin isSubmited={submitted} onSubmitLogin={onSubmitLogin} onChangelogin={onHandleChange}/>
            </ThemeLayout>
        </React.Fragment>
    );
}

// function mapState(state) {
//     const { loggingIn } = state.authentication;
//     return { loggingIn };
// }

// const actionCreators = {
//     login: userActions.login,
//     logout: userActions.logout
// };

// const connectedLoginPage = connect(mapState, actionCreators)(Login);
// export { connectedLoginPage as Login };

export default Login;