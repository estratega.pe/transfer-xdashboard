import React from 'react';
import { ThemeLayout } from '../../theme/estratega/layout';

class Home extends React.Component {
    render() {
        return (
            <React.Fragment>
                <ThemeLayout />
            </React.Fragment>
        );
    }
}

export default Home;