import React from 'react';
import { Link } from 'react-router-dom';
import { ThemePartialAside } from './partials/partial-aside';

export const ThemeMenu = (props)  => {
    return (
        <React.Fragment>
            <ThemePartialAside classname="aside aside-fixed">
                <div className="aside-header">
                    <Link to="/home" className="aside-logo">Transfer</Link>
                    <Link to="" className="aside-menu-link">
                        <i data-feather="menu"></i>
                        <i data-feather="x"></i>
                    </Link>
                </div>
            </ThemePartialAside>
        </React.Fragment>
    );
}