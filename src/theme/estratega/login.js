import React from 'react';
import { Link } from 'react-router-dom';

export const ThemeLogin = ( props ) => {
    return (
        <React.Fragment>
            <div className="content-header">
                <img src="/assets/img/logo-web.png" alt="Logo"/>
            </div>
            <div className="content content-auth">
                <div className="container">
                    <div className="media align-items-stretch justify-content-center ht-100p pos-relative">
                        <div className="media-body align-items-center d-none d-lg-flex">
                            <div className="mx-wd-600">
                                <img src="https://via.placeholder.com/1260x950" className="img-fluid" alt="" />
                            </div>
                        </div>
                        <div className="sign-wrapper mg-lg-l-50 mg-xl-l-60">
                            <div className="wd-100p">
                            <h3 className="tx-color-01 mg-b-5">Sign In</h3>
                            <p className="tx-color-03 tx-16 mg-b-40">Welcome back! Please signin to continue.</p>
                            <form onSubmit={props.onSubmitLogin}>
                            <div className="form-group">
                                <label>Email address</label>
                                <input type="text" name="_username" className="form-control" placeholder="yourname@yourmail.com" onChange={props.onChangelogin} />
                            </div>
                            <div className="form-group">
                                <div className="d-flex justify-content-between mg-b-5">
                                    <label className="mg-b-0-f">Password</label>
                                    <Link to="/recovery-password">Forgot password?</Link>
                                </div>
                                <input type="password" name="_password" className="form-control" placeholder="Enter your password" onChange={props.onChangelogin} />
                            </div>
                            <button className="btn btn-brand-02 btn-block">Sign In</button>
                            </form>
                            {/*<div className="divider-text">or</div>*/}
                                {/*<button className="btn btn-outline-facebook btn-block">Sign In With Facebook</button>
                                <button className="btn btn-outline-twitter btn-block">Sign In With Twitter</button>*/}
                            <div className="tx-13 mg-t-20 tx-center">Don't have an account? <Link to="/register">Create an Account</Link></div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </React.Fragment>
    );
}