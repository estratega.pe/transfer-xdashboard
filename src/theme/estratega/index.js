import React from 'react';
import { ThemeMenu } from './menu';
import { ThemeLayout } from './layout';

export const ThemeIndex = ( props ) => {
    return (
        <React.Fragment>
            <ThemeLayout>
                <ThemeMenu />
                { props.children }
            </ThemeLayout>
        </React.Fragment>
    );
}