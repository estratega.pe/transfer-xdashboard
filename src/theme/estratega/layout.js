import React from 'react';

export const ThemeLayout = ( props ) => {
    return (
        <React.Fragment>
            { props.children }
        </React.Fragment>
    );
}