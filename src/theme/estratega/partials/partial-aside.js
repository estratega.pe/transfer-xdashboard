import React from 'react';

export const ThemePartialAside = (props)  => {
    const { classname } = props;
    return (
        <React.Fragment>
            <aside className={classname}>
                { props.children }
            </aside>
        </React.Fragment>
    );
}