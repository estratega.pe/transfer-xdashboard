  
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import { store } from './helpers';
import { Estratega } from './estratega';

render(
    <Provider store={store}>
        <Estratega />
    </Provider>,
    document.getElementById('root')
);

/*import React from 'react';
import ReactDOM from 'react-dom';
import Estratega from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <Estratega />, document.getElementById('root')
);

serviceWorker.unregister();
*/
